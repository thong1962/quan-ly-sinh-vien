<?php 
require_once('controler/init.php');
require_once('controler/check-cookie.php');
?>
<!DOCTYPE html>
<html lang="en">
<?php require_once('global/head.php');?>
<body>
<?php require_once('global/header.php');?>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      <?php require_once('content/content_left.php');?>
    </div>
    <div class="col-sm-8 text-left"> 
      <?php require_once('content/about.php');?>
    </div>
    <div class="col-sm-2 sidenav">
      <?php require_once('content/content_right.php');?>
    </div>
  </div>
</div>

<?php require_once('global/footer.php');?>
</body>
</html>
