<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
require_once('init.php');
function getUserLogin($email,$pass){
	$servername = DB_HOST;
	$username = DB_USER;
	$password = DB_PASS;
	$dbname = DB_NAME;
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}
	$sql = "SELECT * FROM hocsinhs WHERE email='".$email."' LIMIT 1";
	$result = $conn->query($sql);
	$login = false;
	if ($result->num_rows > 0) {
		$user = $result->fetch_assoc();
		if(password_verify($pass, $user['password'])){
			$_SESSION['user'] = $user;
			$login = true;
		}
	}
	$conn->close();
	return $login;
}

$datas = $_POST;

if(!isset($datas['email']) || empty($datas['email'])){
	header('Location: '.APP_URL.'login.php');
	exit;
}
if(!isset($datas['password']) || empty($datas['password'])){
	header('Location: '.APP_URL.'login.php');
	exit;
}

// abc@gmail.com / 123456
// $password = '123456';
// $crypted = password_hash($password, PASSWORD_DEFAULT);
$user = getUserLogin($datas['email'],$datas['password']);
if($user != false){
	$cookie_name = "user_login";
	$cookie_value = "1";
	setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
	header('Location: '.APP_URL.'index.php');
	exit;
}else{
	$cookie_name = "user_login";
	$cookie_value = "0";
	setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
	header('Location: '.APP_URL.'login.php');
	exit;
}
