<?php
if(isset($_SESSION['user'])){
  $user = $_SESSION['user'];
}
?>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="<?php echo APP_URL?>">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo APP_URL?>">Home</a></li>
        <li><a href="<?php echo APP_URL?>about.php">About</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li>
          <?php if($user){?>
            <a href="#"><span class="glyphicon glyphicon-log-in"></span> Xin chào: <?php echo $user['firstname'].' '.$user['lastname']?></a>
          <?php }else{?>
            <a href="<?php echo APP_URL?>login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a>
          <?php }?><
        </li>
      </ul>
    </div>
  </div>
</nav>