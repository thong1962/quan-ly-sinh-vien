<?php
session_start();
$user_login = null;
if(isset($_COOKIE['user_login'])){
	$user_login = $_COOKIE['user_login'];
	setcookie("user_login", null, time() - 3600,'/');
}
?>
<!DOCTYPE html>
<html lang="en">
<?php require_once('global/head.php');?>
<body>
<?php require_once('global/header.php');?>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      <?php require_once('content/content_left.php');?>
    </div>
    <div class="col-sm-8 text-left"> 
      <?php require_once('content/login.php');?>
      <?php if($user_login == '0'){?>
      	<?php require_once('content/message.php');?>
  	  <?php }?>
    </div>
    <div class="col-sm-2 sidenav">
      <?php require_once('content/content_right.php');?>
    </div>
  </div>
</div>

<?php require_once('global/footer.php');?>
</body>
</html>
